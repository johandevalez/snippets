$('<a class="toggle-link">').html('&nbsp;').prependTo('.toggle').click(function(e) {
  e.preventDefault();
  $(this).siblings('ul').eq(0).slideToggle('fast');
  $(this).parent().toggleClass('expanded');
});