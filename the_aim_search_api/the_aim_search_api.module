<?php
/**
 * Working method for search API:
 * 
 * The search is completely based on the aggregate search field. No other fields will be used for search.
 * To be able to add fields to the aggregate field you have to activate them in the field list first, 
 * and then activate them in the aggregate field itself.
 * And of course you have to activate the aggregate field first as well.
 * 
 * The default machine name for the first created aggregate field is search_api_aggregation_1 and that should be sufficient.
 * Please note that the aggregate field should be named the same for the node search index as for the taxonomy search index.
 * 
 */
/**
 * @var String
 *   the internal path the search page is available at
 */
define('THE_AIM_SEARCH_API_SEARCH_PAGE', 'search');
/**
 * @var String
 *   the machine name of the search API index of nodes
 */
define('THE_AIM_SEARCH_API_SEARCH_INDEX_NODE', 'node_index');
/**
 * @var String
 *   the machine name of the search API index of taxonomy terms
 */
define('THE_AIM_SEARCH_API_SEARCH_INDEX_TAXONOMY', 'taxonomy_index');
/**
 * @var String
 *   the machine name of the search API aggregate search field
 */
define('THE_AIM_SEARCH_API_SEARCH_AGGREGATE_FIELD_MACHINE_NAME', 'search_api_aggregation_1');

/**
 * Implements hook_menu()
 */
function the_aim_search_api_menu() {
  return array(
    THE_AIM_SEARCH_API_SEARCH_PAGE => array(
      'title' => "Search",
      'page callback' => '_the_aim_search_api_search_page',
      'access arguments' => array('access content'),
    ),
  );
}

/**
 * Implements hook_block_info()
 */
function the_aim_search_api_block_info() {
  return array(
    'search_api_block' => array(
      'info' => 'Search API form',
    )
  );
}

/**
 * Implements hook_block_view()
 */
function the_aim_search_api_block_view($delta = '') {
  switch ($delta) {
    case 'search_api_block':
      return array(
        'content' => drupal_get_form('_the_aim_search_api_search_form'),
      );
  }
}

/**
 * Custom page callback for </search>
 */
function _the_aim_search_api_search_page() {
  $search_form = drupal_get_form('_the_aim_search_api_search_form');
  $content = drupal_render($search_form);

  $qry = drupal_get_query_parameters();

  if (key_exists('str', $qry)) {
    $results = _the_aim_search_api_search_results($qry['str']);
    $content .= drupal_render($results);
  }

  return $content;
}

/**
 * Custom search form
 */
function _the_aim_search_api_search_form($form, &$form_state) {
  $form['search_string'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#element_validate' => array('_the_aim_search_api_search_validate_search_query'),
    '#attributes' => array(
      'placeholder' => t('Search')
    ),
    '#default_value' => isset($_GET['str']) ? $_GET['str'] : null,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

/**
 * Search form submit
 */
function _the_aim_search_api_search_form_submit($form, &$form_state) {
  $form_state['redirect'] = array(
    THE_AIM_SEARCH_API_SEARCH_PAGE,
    array(
      'query' => array(
        'str' => $form_state['values']['search_string']
      ),
    )
  );
}

/**
 * Custom search query validation
 *   - can not be empty
 */
function _the_aim_search_api_search_validate_search_query($element, &$form_state, $form) {
  if (empty($element['#value'])) {
    form_error($element, t('This field is required.'));
  }
}

/**
 * Custom function to retrieve a requested qry from the database
 *
 * @param String $qry
 * @return String
 *   the rendered results
 */
function _the_aim_search_api_search_results($qry) {
  global $language;
  
  $return = array();
  $result_list = array();
  $limit = variable_get('search_api_overview_number_of_items', 10);
  $offset = pager_find_page() * $limit;
  $taxonomy_onpage_count = 0;
  $total_result_count = 0;
  
  // TAXONOMY

  $taxonomy_index = search_api_index_load(THE_AIM_SEARCH_API_SEARCH_INDEX_TAXONOMY);
  $is_i18n_active = module_exists('i18n');
  
  if(!empty($taxonomy_index)) {
    try {
      // build the query
      $taxonomy_query = new SearchApiQuery($taxonomy_index);
      $taxonomy_query->condition(THE_AIM_SEARCH_API_SEARCH_AGGREGATE_FIELD_MACHINE_NAME, '%'. $qry .'%', 'LIKE');
      if($is_i18n_active) {
        $taxonomy_query->condition('search_api_language', $language->language);
      }
      $taxonomy_query->range($offset, $limit);

      $result = $taxonomy_query->execute();
    } catch (Exception $ex) {
      trigger_error($ex->getMessage(), E_USER_WARNING );
    }
    
    // count results
    $total_result_count = $result['result count'];
    
    if (isset($result['results']) && !empty($result['results'])) {
      $taxonomy_terms = taxonomy_term_load_multiple(array_keys($result['results']));
      $taxonomy_terms = taxonomy_term_view_multiple($taxonomy_terms, 'search_result');
      
      
      $taxonomy_onpage_count = count($result['results']);
      
      $result_list += $taxonomy_terms['taxonomy_terms'];
    }
  } else {
    trigger_error(t('There is no search API index yet identified by %ID', array('%ID' => THE_AIM_SEARCH_API_SEARCH_INDEX_TAXONOMY)), E_USER_WARNING );
  }
  
  
  // NODES
  $node_index = search_api_index_load(THE_AIM_SEARCH_API_SEARCH_INDEX_NODE);
  // reset results
  $result = null;
  if(!empty($node_index) && $limit > $taxonomy_onpage_count) {
    try {
      // update offset
      $offset = ($offset - $total_result_count < 0)?0:$offset - $total_result_count;
      // build the query
      $query = new SearchApiQuery($node_index);
      $query->condition(THE_AIM_SEARCH_API_SEARCH_AGGREGATE_FIELD_MACHINE_NAME, '%'. $qry .'%', 'LIKE');
      if($is_i18n_active) {
        $query->condition('search_api_language', $language->language);
      }
      $query->range($offset, $limit - $taxonomy_onpage_count);

      $result = $query->execute();
    } catch (Exception $ex) {
      trigger_error($ex->getMessage(), E_USER_WARNING );
    }
    
    if (isset($result['results']) && !empty($result['results'])) {
      // generate the result(s)
      $nodes = node_load_multiple(array_keys($result['results']));
      $nodes = node_view_multiple($nodes, 'search_result');

      // add the result count
      $total_result_count += $result['result count'];

      $result_list += $nodes['nodes'];
    }
  } 
  elseif (empty($node_index)) {
    trigger_error(t('There is no search API index yet identified by %ID', array('%ID' => THE_AIM_SEARCH_API_SEARCH_INDEX_NODE)), E_USER_WARNING );
  }

  if (!empty($result_list)) {
    // init the pager
    pager_default_initialize($total_result_count, $limit);
    // pass the loaded entities to the template
    $return[] = array(
      '#theme' => 'search_api_search_results',
      '#items' => $result_list,
    );
    $return[] = array(
      '#theme' => 'pager',
    );
    return $return;
  }

  return array(
    '#theme' => 'html_tag',
    '#tag' => 'div',
    '#value' => t("No values found for your search query"),
  );
}

/**
 * Implements hook_theme()
 */
function the_aim_search_api_theme() {
  return array(
    'search_api_search_results' => array(
      'template' => 'search-api-search-results',
      'variables' => array(
        'items' => NULL,
      ),
    ),
  );
}
