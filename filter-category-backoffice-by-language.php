<?php
/**
 * Implements hook_form_alter
 */
function the_aim_custom_form_alter(&$form, &$form_state, $form_id) {
  global $language;
  
  // List of field names that are possible term reference fields
  $term_ref_fields = array(
    'field_category'
  );
  
  $existing_fields = array_flip(array_intersect_key(array_flip($term_ref_fields), $form));

  foreach($existing_fields as $field_name) {
    // only show terms for current language
    $ln = $language->language;
    if(!empty($form['#entity']->language)) {
      $ln = $form['#entity']->language;
    }
    
    $tid_list = array_keys($form[$field_name][LANGUAGE_NONE]['#options']);

    $taxonomyQuery = db_select('taxonomy_term_data', 'tt');
    //$taxonomyQuery->join('taxonomy_term_hierarchy', 'th', 'th.tid = tt.tid');
    $taxonomyQuery
      ->fields('tt', array('tid'))
      ->condition('tt.tid', $tid_list, 'IN')
      ->condition('tt.language', $ln)
      //->condition('th.parent', 0, '<>')
      ->orderBy('weight');
    $taxonomyTerms = $taxonomyQuery->execute()->fetchAllAssoc('tid');
    $form[$field_name][LANGUAGE_NONE]['#options'] = array_intersect_key($form[$field_name][LANGUAGE_NONE]['#options'], $taxonomyTerms);
  }
}