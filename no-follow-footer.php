<?php
function the_aim_theme_menu_link(array $variables) {
  $element = $variables['element'];

  // add rel=nofollow to footer links
  if ($element['#original_link']['menu_name'] == 'footer-menu') {
    $element['#localized_options']['attributes']['rel'] = 'nofollow';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . "</li>\n";
}