$(document).ready(function() {
  /* grunticon Stylesheet Loader | https://github.com/filamentgroup/grunticon | (c) 2012 Scott Jehl, Filament Group, Inc. | MIT license. */
  window.grunticon=function(e){if(e&&2===e.length){var t=window,n=!(!t.document.createElementNS||!t.document.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect||!document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")||window.opera&&-1===navigator.userAgent.indexOf("Chrome")),o=function(o){var r=t.document.createElement("link"),a=t.document.getElementsByTagName("script")[0];r.rel="stylesheet",r.href=e[o && n ? 0 : 1],a.parentNode.insertBefore(r,a)},r=new t.Image;r.onerror=function(){o(!1)},r.onload=function(){o(1===r.width&&1===r.height)},r.src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="}};
  grunticon( [ Drupal.settings.basePath + '' + Drupal.settings.themePath + '/css/icons.data.svg.css', Drupal.settings.basePath + Drupal.settings.themePath + '/css/icons.data.png.css' ] );
});
/*
 * Requires:
 * function jojo_preprocess_html(&$variables) {
 *   // add theme path to drupal settings
 *   drupal_add_js(array('themePath' => path_to_theme()), 'setting');
 * }
 */