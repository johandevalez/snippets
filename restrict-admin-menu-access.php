<?php

/**
 * Implements hook_menu_alter
 */
function the_aim_custom_menu_alter(&$items) {
  // this adds an access check for the manage display pages
  $items['user/%user/display']['access arguments'] = array('use manage display');
  $items['node/%node/display']['access arguments'] = array('use manage display');
  $items['taxonomy/term/%taxonomy_term/display']['access arguments'] = array('use manage display');

  // redirect default /node to frontpage
  $items['node']['page callback'] = 'drupal_goto';
  $items['node']['page arguments'] = array('<front>');
}

/**
 * Implements hook_permission
 */
function the_aim_custom_permission() {
  return array(
    // add an extra access check to manage display page
    'use manage display' => array(
      'title' => t('Use manage display'),
    ),
  );
}