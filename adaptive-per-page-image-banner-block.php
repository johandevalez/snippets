<?php
/***!!! Add cs_adaptive_image module dependency when using this !!!***/
/**
 * Implements hook_block_info()
 */
function the_aim_custom_block_info() {
  return array(
    'custom-image-banner' => array(
      'info' => t("Page Image Banner"),
      'cache' => DRUPAL_CACHE_PER_PAGE,
    )
  );
}

/**
 * Implements hook_block_view()
 */
function the_aim_custom_block_view($delta = '') {
  switch ($delta) {
    case 'custom-image-banner':
      return array(
        'content' => _custom_image_banner(),
      );
  }
}

/**
 * Load in page banner image
 */
function _custom_image_banner() {
  $field = array();
  $field_name = 'field_image_banner'; // name of image field to use as banner
  // get node
  $object = menu_get_object();
  // get taxonomy if not a node page
  
  if($object) {
    $field = field_view_field('node', $object, $field_name);
  }
  else {
    $object = menu_get_object('taxonomy_term', 2);
    $field = field_view_field('taxonomy_term', $object, $field_name);
  }

  if(!empty($field)) {
    // Hide label
    $field['#label_display'] = 'hidden';
    // Update image to adaptive
    $field['#formatter'] = 'cs_adaptive_image';
    $field[0]['#theme'] = 'cs_adaptive_image_formatter';
    $field[0]['#fallback_style'] = 'large'; // add fallback image style
    $field[0]['#breakpoint_styles'] = array(
      // break => image_style ** add as required
      'default' => 'medium',
    );
  }
  
  return $field;
}