<?php

function the_aim_theme_preprocess_html(&$variables) {
  // update head title
  // 1. for taxonomy
  if (isset($variables['page']['content']['system_main']['term_heading']['term']['#term']->the_aim_seo_pagetitle[LANGUAGE_NONE][0])) {
    $variables['head_title'] = $variables['page']['content']['system_main']['term_heading']['term']['#term']->the_aim_seo_pagetitle[LANGUAGE_NONE][0]['safe_value'];
  }
  // 2. for frontpage
  elseif (drupal_is_front_page() && isset($variables['page']['content']['system_main']['nodes'])) {
    $page_node = current($variables['page']['content']['system_main']['nodes']);
    $variables['head_title'] = $page_node['#node']->the_aim_seo_pagetitle[LANGUAGE_NONE][0]['safe_value'] . ' | ' . $variables['head_title_array']['name'];
  }
}
