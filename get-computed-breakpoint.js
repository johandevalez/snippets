// computed breakpoint
var breakpoint = '';
var newbreakpoint = 'default';
function getComputedBreakpoint() {
  // check for new breakpoint;
  if(window.getComputedStyle) {
    newbreakpoint = window.getComputedStyle(document.body, ':after').getPropertyValue('content');
    newbreakpoint = newbreakpoint.replace(/"/g, "");
    newbreakpoint = newbreakpoint.replace(/'/g, "");
  }
  if(!newbreakpoint || newbreakpoint == breakpoint) {
    return false;
  }
  breakpoint = newbreakpoint;
  return true;
}
getComputedBreakpoint();

// resize
$(window).resize(function() {

    // EVERYTHING RELATED TO BREAKPOINTS COMES BENEATH !!!
    // we need to keep it running as clean as possible
    if(getComputedBreakpoint()) {
      
    }
});