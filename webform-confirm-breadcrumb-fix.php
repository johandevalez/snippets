<?php

/**
 * Implements hook_page_delivery_callback_alter().
 *
 * We're using this hook because menu_trail_by_path uses it as well
 * and we need to adjust our breadcrumbs
 */
function the_aim_custom_page_delivery_callback_alter() {
// Check if we're on a webform confirmation page
  $path = explode('/', current_path());
  if (count($path) == 3 && $path[0] == 'node' && is_numeric($path[1]) && $path[2] == 'done') {
    $breadcrumb = array(
      l(t('Home'), '<front>'),
    );

    drupal_set_breadcrumb($breadcrumb);
  }
}
