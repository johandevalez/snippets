<?php

  // BLOCK TEMPLATE

?>
<?php if (!$no_wrapper): ?>
  <<?php print $tag; ?> <?php print drupal_attributes($attributes_array); ?>>
<?php endif; ?>

<?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
  <p class="title"><?php print $block->subject ?></p>
<?php endif; ?>
<?php print render($title_suffix); ?>

<?php print $content ?>

<?php if (!$no_wrapper): ?>
  </<?php print $tag; ?>>
<?php endif; ?>

<?php

/**
 * Implements hook_preprocess_block => belongs in template.php
 * @param type $variables
 */
function the_aim_theme_preprocess_block(&$variables) {
  // Initialize
  $variables['tag'] = 'div';
  $variables['no_wrapper'] = FALSE;
  $menu_block_modules = array(
    'menu',
    'menu_block',
    'taxonomy_menu_block'
  );
  
  if (in_array($variables['block']->module, $menu_block_modules)) {
    $variables['tag'] = 'nav';
  }

  // Check if we have any classes at all
  if ($variables['classes_array']) {
    // Prevent the wrapper from rendering
    if (in_array('<none>', $variables['classes_array'])) {
      // Don't wrap the content
      $variables['no_wrapper'] = TRUE;
      // Remove the <none> class from the list
      $variables['classes_array'] = array_diff($variables['classes_array'], array('<none>'));
    }

    // Show contextual links inside the wrapper with the original classes
    if (in_array('contextual-links-region', $variables['classes_array']) && $variables['no_wrapper'] === TRUE) {
      // Add the wrapper again
      $variables['no_wrapper'] = FALSE;
    }

    // Change the wrapper tag
    if (preg_match('/(\<tag\:){1}([a-z]*){1}(\>){1}/', implode(' ', $variables['classes_array']), $matches)) {
      // Set the tag for the wrapper around the content
      $variables['tag'] = $matches[2];
      // Remove the <tag:*> class from the list
      $variables['classes_array'] = array_diff($variables['classes_array'], array($matches[0]));
    }

    // Set the class attribute
    $variables['attributes_array']['class'] = $variables['classes_array'];
  }
}