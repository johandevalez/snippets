<?php

/**
 * Implements hook_block_view_alter
 */
function the_aim_custom_block_view_alter(&$data, $block) {
  // let menu blocks h3 title follow the active item
  $menu_match = array('main-menu');
  if (isset($data['content']['#config']['menu_name']) && isset($data['content']['#content']) && in_array($data['content']['#config']['menu_name'], $menu_match)) {
    $active_trail = menu_get_active_trail();
    $current_level = $data['content']['#config']['level'];
    foreach ($data['content']['#content'] as $item) {
      if (
        isset($item['#original_link']['link_path']) && $item['#original_link']['depth'] == $current_level 
        && (
          isset($active_trail[$current_level]['link_path']) && $item['#original_link']['link_path'] == $active_trail[$current_level]['link_path'] 
          || drupal_is_front_page() && isset($active_trail[0]['link_path']) && $item['#original_link']['link_path'] == $active_trail[0]['href']
        )
      ) {
        $block->title = $item['#original_link']['link_title'];
      }
    }
  }
}
