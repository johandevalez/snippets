<?php

/**
 * Implementation of hook_block_info().
 */
function the_aim_custom_block_info() {
  $blocks = array();

  $blocks['back-to-overview'] = array(
    'info' => t('Back to overview'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );

  return $blocks;
}

/**
 * Implementation of hook_block_view().
 */
function the_aim_custom_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    // NOTE: back to overview links will only work correctly when the right alias and patterns have been set
    case 'back-to-overview':
      // get path alias
      $path_alias = explode('/', drupal_lookup_path('alias', current_path()));
      // remove the last url section
      array_pop($path_alias);
      // get the original path for the url, if it exists
      $mainpath = drupal_lookup_path('source', implode('/', array_filter($path_alias)));
      $content = '';

      // Blocks won't show when content is empty, so don't add content when there is no valid back page found.
      if (!empty($mainpath)) {
        // make link
        $content = '<p>' . l(t('Back to overview'), $mainpath, array('attributes' => array('title' => t('Back to overview')))) . '</p>';
        // assign block content
        $block['content'] = $content;
      }
      break;
  }
  return $block;
}
