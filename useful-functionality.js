(function ($) {
  // *** CTA UX ***
  // keep original functionality of native links
  $('.cta a').click(function(e) {
    e.stopPropagation();
  });
  // add link functionality to full CTA block
  $('.cta').click(function(e) {
    var elems = $(this).find('> a');
    if(elems.length == 0) {
      elems = $(this).children().not('.contextual-links-wrapper').find('a');
    }
    if(elems.length > 0) {
      window.location = elems.eq(0).attr('href');
    }
  });
})(jQuery);