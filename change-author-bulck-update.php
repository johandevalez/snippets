<?php
/**
 * Implementation of hook_node_operations().
 * Add siteowner users to list to easily change author to.
 */
function the_aim_custom_node_operations() {
  // Get the correct role data
  $role = user_role_load_by_name('siteowners');
  // Get siteowner users
  $query = db_select('users_roles', 'ur');
  $query->innerJoin('users', 'u', 'ur.uid = u.uid AND ur.rid = :rid', array(':rid' => $role->rid));
  $results = $query
    ->fields('ur', array('uid'))
    ->fields('u', array('name'))
    ->execute()
    ->fetchAllAssoc('uid');
  $operations = array();

  foreach($results as $uid => $result) {
    $operations['author_update_so_' . $result->uid] = array(
      'label' => t('Change author to !name', array('!name' => $result->name)),
      'callback' => 'node_mass_update',
      'callback arguments' => array('updates' => array('uid' => $result->uid)),
    );
  }
  
  return $operations;
}