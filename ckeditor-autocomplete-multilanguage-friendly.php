<?php
/**
 * Implements hook_ckeditor_link_autocomplete_alter()
 */
function the_aim_custom_ckeditor_link_autocomplete_alter(&$results, &$string) {
  
  $result_paths = array_keys($results);
  $node_links = preg_grep('/node\//', $result_paths);
  $taxonomy_links = preg_grep('/taxonomy\/term\//', $result_paths);
  // see in ckeditor_link_autocomplete()
  $limit = variable_get('ckeditor_link_limit', 10);
  // update autocomplete for taxonomy and node, to better support multilanguage
  if(module_exists('translation')) {
    _the_aim_custom_ckeditor_link_node_autocomplete_custom($results, $string, $node_links, $limit);
  }
  if(module_exists('i18n_taxonomy')) {
    _the_aim_custom_ckeditor_link_taxonomy_autocomplete_custom($results, $string, $taxonomy_links, $limit);
  }
}

/**
 * Updated ckeditor internal path autocomplete for nodes
 * Original function: ckeditor_link_ckeditor_link_node_autocomplete
 */
function _the_aim_custom_ckeditor_link_node_autocomplete_custom(&$results, $string, $node_links, $limit) {
  // filter out all node links
  $results = array_diff_key($results, array_flip($node_links));
  $matches = array();
  
  $node_types = array_keys(array_filter(variable_get('ckeditor_link_autocomplete_node_types', array('- any -' => '- any -'))));
  if (count($node_types)) {
    //we assume i18n is enabled
    global $language;
    $db_or = db_or();
    $db_or->condition('n.language', LANGUAGE_NONE);
    $db_or->condition('n.language', $language->language);
    $query = db_select('node', 'n')
      ->fields('n', array('nid', 'title', 'language'))
      ->condition('n.title', '%' . db_like($string) . '%', 'LIKE')
      ->condition($db_or)
      ->orderBy('n.title')
      ->orderBy('n.type')
      ->range(0, $limit)
      ->addTag('node_access');
    if (!in_array('- any -', $node_types)) {
      $query->condition('n.type', $node_types, 'IN');
    }
    $result = $query->execute();
    foreach ($result as $node) {
      $matches['node/' . $node->nid] = $node->title . " [" . $node->language . "]";
    }
  }
  $results += $matches;
}

/**
 * Updated ckeditor internal path autocomplete for taxonomy terms
 * Original function: ckeditor_link_ckeditor_link_taxonomy_autocomplete
 */
function _the_aim_custom_ckeditor_link_taxonomy_autocomplete_custom(&$results, $string, $taxonomy_links, $limit) {
  // filter out all taxonomy links
  $results = array_diff_key($results, array_flip($taxonomy_links));
  $matches = array();

  $vocabularies = array_keys(array_filter(variable_get('ckeditor_link_autocomplete_vocabularies', array())));
  if (count($vocabularies)) {
    //we assume i18n is enabled
    global $language;
    $db_or = db_or();
    $db_or->condition('t.language', LANGUAGE_NONE);
    $db_or->condition('t.language', $language->language);
    $query = db_select('taxonomy_term_data', 't')
      ->fields('t', array('tid', 'name', 'language'))
      ->condition('t.name', '%' . db_like($string) . '%', 'LIKE')
      ->condition($db_or)
      ->orderBy('t.name')
      ->range(0, $limit)
      ->addTag('term_access');
    if (!in_array('- any -', $vocabularies)) {
      $query->condition('t.vid', $vocabularies, 'IN');
    }
    $result = $query->execute();
    foreach ($result as $term) {
      $matches['taxonomy/term/' . $term->tid] = $term->name . " [" . $term->language . "]";
    }
  }

  $results += $matches;
}