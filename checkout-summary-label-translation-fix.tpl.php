<?php
/**
 * Implements commerce_price_formatted_amount preprocessing
 */
function the_aim_theme_commerce_preprocess_commerce_price_formatted_components(&$variables) {
  $variables['components']['commerce_price_formatted_amount']['title'] = t('Order total');
}