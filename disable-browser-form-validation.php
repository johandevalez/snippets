<?php

/**
 * Implements hook_form_alter
 */
function the_aim_custom_form_alter(&$form, &$form_state, $form_id) {
  // Form IDs to exclude from default html5 browser validation.
  // The form ID can easily be found in the form html itself as a hidden field.
  $webvalidation_exclude_forms = array(
    'webform_client_form_12'
  );
  if(in_array($form_id, $webvalidation_exclude_forms)) {
    $form['#attributes']['novalidate'] = 'novalidate';
  }
}